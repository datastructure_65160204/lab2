public class ArrayDeletionsLab {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};

        System.out.print("Original Array: ");
        for(int i=0;i<arr.length;i++) {
            System.out.print(arr[i]+" ");
        }
        System.out.println();

        int indexToDelete = 2;
        arr = deleteElementByIndex(arr, indexToDelete);
        System.out.print("Array after deleting element at index " + indexToDelete + ": ");
        for(int i=0;i<arr.length;i++) {
            System.out.print(arr[i]+" ");
        }
        System.out.println();

        int valueToDelete = 4;
        arr = deleteElementByValue(arr, valueToDelete);
        System.out.print("Array after deleting element with value " + valueToDelete + ": ");
        for(int i=0;i<arr.length;i++) {
            System.out.print(arr[i]+" ");
        }
    }

    public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            return arr;
        }

        int[] newArr = new int[arr.length - 1];
        for (int i = 0, j = 0; i < arr.length; i++) {
            if (i == index) {
                continue;
            }
            newArr[j] = arr[i];
            j++;
        }

        return newArr;
    }

    public static int[] deleteElementByValue(int[] arr, int value) {
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            return arr;
        }

        return deleteElementByIndex(arr, index);
    }
}
