public class ArrayRemoveDuplicatesLab {

    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        int k = 1; // จำนวนของสมาชิกที่เป็นเอกลักษณ์

        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[i - 1]) {
                nums[k] = nums[i];
                k++;
            }
        }

        return k;
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 1, 2};

        System.out.print("Input: nums = ");
        for(int i=0;i<nums1.length;i++) {
            System.out.print(nums1[i]+" ");
        }
        System.out.println();

        int k1 = removeDuplicates(nums1);
        System.out.print("Output: " + k1 + ", nums = [");
        for (int i = 0; i < k1; i++) {
            System.out.print(nums1[i]);
            if (i < k1 - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");

        int[] nums2 = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};

        System.out.print("Input: nums = ");
        for(int i=0;i<nums2.length;i++) {
            System.out.print(nums2[i]+" ");
        }
        System.out.println();

        int k2 = removeDuplicates(nums2);
        System.out.print("Output: " + k2 + ", nums = [");
        for (int i = 0; i < k2; i++) {
            System.out.print(nums2[i]);
            if (i < k2 - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }
}
